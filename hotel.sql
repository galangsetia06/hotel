-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versi server:                 10.4.24-MariaDB - mariadb.org binary distribution
-- OS Server:                    Win64
-- HeidiSQL Versi:               11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Membuang struktur basisdata untuk hotel
CREATE DATABASE IF NOT EXISTS `hotel` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `hotel`;

-- membuang struktur untuk table hotel.kamar
CREATE TABLE IF NOT EXISTS `kamar` (
  `id_kamar` int(11) NOT NULL,
  `No_Kamar` varchar(50) NOT NULL,
  `Nama` varchar(50) NOT NULL,
  `Harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Membuang data untuk tabel hotel.kamar: ~12 rows (lebih kurang)
/*!40000 ALTER TABLE `kamar` DISABLE KEYS */;
INSERT INTO `kamar` (`id_kamar`, `No_Kamar`, `Nama`, `Harga`) VALUES
	(1, '1A', 'Mawar', 200000),
	(2, '1B', 'Melati', 250000),
	(3, '1C', 'Semanggi', 230000),
	(3, '2A', 'Lotus', 300000),
	(4, '2B', 'Kamboja', 450000),
	(5, '2C', 'Monstera', 340000),
	(6, '3A', 'Asoka', 530000),
	(7, '3B', 'Lily', 750000),
	(8, '3C', 'Alamanda', 650000),
	(9, '4A', 'Lavender', 1000000),
	(10, '4B', 'Bougenville', 1350000),
	(11, '4C', '7 Rupa', 0);
/*!40000 ALTER TABLE `kamar` ENABLE KEYS */;

-- membuang struktur untuk table hotel.listpaymentmethod
CREATE TABLE IF NOT EXISTS `listpaymentmethod` (
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Membuang data untuk tabel hotel.listpaymentmethod: ~2 rows (lebih kurang)
/*!40000 ALTER TABLE `listpaymentmethod` DISABLE KEYS */;
INSERT INTO `listpaymentmethod` (`nama`) VALUES
	('Cash'),
	('Non Cash');
/*!40000 ALTER TABLE `listpaymentmethod` ENABLE KEYS */;

-- membuang struktur untuk table hotel.reservasi
CREATE TABLE IF NOT EXISTS `reservasi` (
  `ID_Reservasi` varchar(50) NOT NULL DEFAULT '',
  `ID_Kamar` int(11) NOT NULL DEFAULT 0,
  `ID_Tamu` varchar(50) NOT NULL,
  `Tanggal_masuk` varchar(50) NOT NULL DEFAULT '',
  `Tanggal_keluar` varchar(50) NOT NULL DEFAULT '',
  `Harga` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Membuang data untuk tabel hotel.reservasi: ~1 rows (lebih kurang)
/*!40000 ALTER TABLE `reservasi` DISABLE KEYS */;
INSERT INTO `reservasi` (`ID_Reservasi`, `ID_Kamar`, `ID_Tamu`, `Tanggal_masuk`, `Tanggal_keluar`, `Harga`) VALUES
	('1', 10, '1', 'Jun 01, 2022', 'Jun 02, 2022', 1350000);
/*!40000 ALTER TABLE `reservasi` ENABLE KEYS */;

-- membuang struktur untuk table hotel.staff
CREATE TABLE IF NOT EXISTS `staff` (
  `id_staff` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_depan` varchar(50) NOT NULL,
  `nama_belakang` varchar(50) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `no_tlp` varchar(50) NOT NULL,
  PRIMARY KEY (`id_staff`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Membuang data untuk tabel hotel.staff: ~1 rows (lebih kurang)
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;
INSERT INTO `staff` (`id_staff`, `username`, `password`, `nama_depan`, `nama_belakang`, `alamat`, `no_tlp`) VALUES
	('1', 'admin', '1234', 'First', 'Admin', '---', '---');
/*!40000 ALTER TABLE `staff` ENABLE KEYS */;

-- membuang struktur untuk table hotel.tamu
CREATE TABLE IF NOT EXISTS `tamu` (
  `id_tamu` varchar(50) NOT NULL,
  `nama_depan` varchar(50) NOT NULL,
  `nama_belakang` varchar(50) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `no_tlp` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Membuang data untuk tabel hotel.tamu: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `tamu` DISABLE KEYS */;
/*!40000 ALTER TABLE `tamu` ENABLE KEYS */;

-- membuang struktur untuk table hotel.tipepembayaran
CREATE TABLE IF NOT EXISTS `tipepembayaran` (
  `idReservasi` int(11) NOT NULL,
  `jenisPembayaran` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Membuang data untuk tabel hotel.tipepembayaran: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `tipepembayaran` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipepembayaran` ENABLE KEYS */;

-- membuang struktur untuk table hotel.transaksi
CREATE TABLE IF NOT EXISTS `transaksi` (
  `ID_Transaksi` int(11) NOT NULL,
  `ID_Reservasi` varchar(50) NOT NULL DEFAULT '',
  `ID_Staff` varchar(50) NOT NULL,
  `Total_pembayaran` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Membuang data untuk tabel hotel.transaksi: ~0 rows (lebih kurang)
/*!40000 ALTER TABLE `transaksi` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaksi` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
