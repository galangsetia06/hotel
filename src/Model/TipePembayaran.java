
package Model;

/**
 *
 * @author ASUS
 */
public class TipePembayaran {
    private String idReservasi;
    private String jenisPembayaran;
    private String noRekening;
    
    public TipePembayaran(){
    
    }
    public TipePembayaran(String idReservasi, String jenisPembayaran) {
        this.idReservasi = idReservasi;
        this.jenisPembayaran = jenisPembayaran;
        this.noRekening = noRekening;
        
    }

    public String getNoRekening() {
        return noRekening;
    }

    public void setNoRekening(String noRekening) {
        this.noRekening = noRekening;
    }

    public String getIdReservasi() {
        return idReservasi;
    }

    public void setIdReservasi(String idReservasi) {
        this.idReservasi = idReservasi;
    }

    public String getJenisPembayaran() {
        return jenisPembayaran;
    }

    public void setJenisPembayaran(String jenisPembayaran) {
        this.jenisPembayaran = jenisPembayaran;
    }
    
}
