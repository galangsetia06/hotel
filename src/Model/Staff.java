package Model;

public class Staff {
    private String id_staff;
    private String username;
    private String password;
    private String nama_depan;
    private String nama_belakang;
    private String alamat;
    private String no_telepon;
   
    public Staff() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNo_telepon() {
        return no_telepon;
    }

    public void setNo_telepon(String no_telepon) {
        this.no_telepon = no_telepon;
    }

    public Staff(String id_staff, String username, String password, String nama_depan, String nama_belakang, String alamat, String no_tlp) {
        this.id_staff = id_staff;
        this.username = username;
        this.password = password;
        this.nama_depan = nama_depan;
        this.nama_belakang = nama_belakang;
        this.alamat = alamat;
        this.no_telepon = no_telepon;
        
    }

    
    public String getId_staff() {
        return id_staff;
    }

    public void setId_staff(String id_staff) {
        this.id_staff = id_staff;
    }

    public String getNama_depan() {
        return nama_depan;
    }

    public void setNama_depan(String nama_depan) {
        this.nama_depan = nama_depan;
    }

    public String getNama_belakang() {
        return nama_belakang;
    }

    public void setNama_belakang(String nama_belakang) {
        this.nama_belakang = nama_belakang;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNo_tlp() {
        return no_telepon;
    }

    public void setNo_tlp(String no_tlp) {
        this.no_telepon = no_tlp;
    }
}
    
