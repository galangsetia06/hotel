/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;



/**
 *
 * @author asus
 */
public class Transaksi {

    private int ID_Transaksi;
    private String ID_Reservasi;
    private String ID_Staff;
    private int Total_pembayaran;

    public Transaksi() {
    }

    public Transaksi(int ID_Transaksi, String ID_Reservasi, String ID_Staff, int Total_pembayaran) {
        this.ID_Transaksi = ID_Transaksi;
        this.ID_Reservasi = ID_Reservasi;
        this.ID_Staff = ID_Staff;
        this.Total_pembayaran = Total_pembayaran;
    }

     public int getID_Transaksi() {
        return ID_Transaksi;
    }

    public void setID_Transaksi(int ID_Transaksi) {
        this.ID_Transaksi = ID_Transaksi;
    }
    public String getID_Reservasi() {
        return ID_Reservasi;
    }

    public void setID_Reservasi(String ID_Reservasi) {
        this.ID_Reservasi = ID_Reservasi;
    }

    public String getID_Staff() {
        return ID_Staff;
    }

    public void setID_Staff(String ID_Staff) {
        this.ID_Staff = ID_Staff;
    }
    public int getTotal_pembayaran() {
        return Total_pembayaran;
    }

    public void setTotal_pembayaran(int Total_pembayaran) {
        this.Total_pembayaran = Total_pembayaran;
    }

   
  
}


