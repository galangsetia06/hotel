/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;

/**
 *
 * @author asus
 */
public class Reservasi {
    private String ID_Reservasi;
    private String ID_Tamu;
    private int ID_Kamar;
    private String Tanggal_masuk;
    private String Tanggal_keluar;
    private int Harga;

    public Reservasi() {
    }
    
     public Reservasi(String ID_Reservasi, String ID_Tamu, int ID_Kamar, String Tanggal_masuk, String Tanggal_keluar, int Harga) {
        this.ID_Tamu = ID_Reservasi;
        this.ID_Tamu = ID_Tamu;
        this.ID_Kamar = ID_Kamar;
        this.Tanggal_masuk = Tanggal_masuk;
        this.Tanggal_keluar = Tanggal_keluar;
        this.Harga = Harga;
     }

    public String getID_Reservasi() {
        return ID_Reservasi;
    }

    public void setID_Reservasi(String ID_Reservasi) {
        this.ID_Reservasi = ID_Reservasi;
    }

    public String getID_Tamu() {
        return ID_Tamu;
    }

    public void setID_Tamu(String ID_Tamu) {
        this.ID_Tamu = ID_Tamu;
    }

    public String getTanggal_masuk() {
        return Tanggal_masuk;
    }

    public void setTanggal_masuk(String Tanggal_masuk) {
        this.Tanggal_masuk = Tanggal_masuk;
    }

    public String getTanggal_keluar() {
        return Tanggal_keluar;
    }

    public void setTanggal_keluar(String Tanggal_keluar) {
        this.Tanggal_keluar = Tanggal_keluar;
    }

    public int getID_Kamar() {
        return ID_Kamar;
    }

    public void setID_Kamar(int ID_Kamar) {
        this.ID_Kamar = ID_Kamar;
    }

    public int getHarga() {
        return Harga;
    }

    public void setHarga(int Harga) {
        this.Harga = Harga;
    }

    
}