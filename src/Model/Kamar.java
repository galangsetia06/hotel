package Model;
public class Kamar {
    private int id_kamar;
    private String no_kamar;
    private String Nama;
    private int harga;
    

    public Kamar() {
    }

    public Kamar(int id_kamar,String no_kamar, String Nama, int harga) {
        this.id_kamar = id_kamar;
        this.no_kamar = no_kamar;
        this.Nama = Nama;
        this.harga = harga;
    }
    
    public int getId_kamar(){
        return id_kamar;
    }
    
    public void setIdKamar(int id_kamar){
        this.id_kamar = id_kamar;
    }
    
    public String getNoKamar() {
        return no_kamar;
    }

    public void setNoKamar(String no_kamar) {
        this.no_kamar = no_kamar;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String Nama) {
        this.Nama = Nama;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }   
}
