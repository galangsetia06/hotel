/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Reservasi;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author asus
 */
public class ControllerReservasi {
    ConnectionManager conMan = new ConnectionManager();
    Connection con = conMan.LogOn();
   
    
    public int getTotal(int ID_Kamar){
        int Total = 0;
        String query = "SELECT Harga from kamar WHERE id_kamar = "+ID_Kamar;
        
        try {
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery(query);
            while(rs.next()){
                Total = rs.getInt("Harga");
            }
        } catch (SQLException e) {
            
            System.out.println(e.toString());
        }
        return Total;
    }
    public int getIdKamar(String nama){
        int i = 0;
        String query = "SELECT * FROM kamar WHERE Nama = '"+nama+"'";
        try {
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery(query);
            while(rs.next()){
                i = rs.getInt("id_kamar");
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        return i;
    }
    public String getNamaKamar(int id){
        String nama = "";
        String query = "SELECT * FROM kamar WHERE id_kamar = '"+id+"'";
        try {
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery(query);
            while(rs.next()){
                nama = rs.getString("Nama");
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        return nama;
    }
    public void insert(String ID_Reservasi,String ID_Tamu, int ID_Kamar, String Tanggal_masuk, String Tanggal_keluar, int Harga){
        String query = "INSERT INTO reservasi (ID_Reservasi, ID_Tamu, ID_Kamar, "
                + "Tanggal_masuk, Tanggal_keluar, Harga) values"
                + "('" + ID_Reservasi + "','" + ID_Tamu + "'," + ID_Kamar + ",'" + Tanggal_masuk + "',"
                + "'" + Tanggal_keluar + "'," + Harga + ")";
        
        try{
            Statement stm = con.createStatement();
            int result = stm.executeUpdate(query);
    }   catch (SQLException ex){
            System.out.println(ex);
            System.out.println("Input Gagal");
    }
}   
    public List<Reservasi> tampil(){
      List<Reservasi> listReservasi = new ArrayList();
         try{
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM reservasi");
            
            while (rs.next()){
                Reservasi Reservasi = new Reservasi();
                Reservasi.setID_Reservasi(rs.getString("ID_Reservasi"));
                Reservasi.setID_Tamu(rs.getString("ID_Tamu"));
                Reservasi.setID_Kamar(rs.getInt("ID_Kamar"));
                Reservasi.setTanggal_masuk(rs.getString("Tanggal_masuk"));
                Reservasi.setTanggal_keluar(rs.getString("Tanggal_keluar"));
                Reservasi.setHarga(rs.getInt("Harga"));
                listReservasi.add(Reservasi); 
            }
         } catch (SQLException ex){
         System.out.println(ex.toString());
         }
         return listReservasi;
    }

  public void update(String ID_Reservasi, String Tanggal_masuk, String Tanggal_keluar, int Harga){
        
        int result = 0;
        String query = "UPDATE reservasi SET Tanggal_masuk = '"+ Tanggal_masuk +"',"
                + "Tanggal_keluar = '"+ Tanggal_keluar +"', Harga = "+ Harga +" WHERE ID_Reservasi = '"+ ID_Reservasi +"';";
        try {
            
            Statement stm = con.createStatement();
            result = stm.executeUpdate(query);
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
    }
    
    public void delete(String ID_Reservasi) {
        
        int result = 0;
        String query = "DELETE FROM reservasi WHERE ID_Reservasi = '" + ID_Reservasi + "'";
        try {
            
            Statement stm = con.createStatement();
            result = stm.executeUpdate(query);
        } catch (SQLException ex) {
            
            System.out.println(ex.toString());
        }
    }

}
