package Controller;
import Model.Tamu;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
public class ControllerTamu {
    Scanner in = new Scanner(System.in);
    ConnectionManager conMan = new ConnectionManager();
    Connection con = conMan.LogOn();
    
    public List<Tamu> read() {
        List<Tamu> listTamu = new ArrayList();
        try {
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM tamu");
            while(rs.next()) {
                Tamu tamu = new Tamu();
                tamu.setIdTamu(rs.getString("id_tamu"));
                tamu.setNamaDepan(rs.getString("nama_depan"));
                tamu.setNamaBelakang(rs.getString("nama_belakang"));
                tamu.setAlamat(rs.getString("alamat"));
                tamu.setNo_telepon(rs.getString("no_tlp"));
                listTamu.add(tamu);
            }
        } catch (SQLException ex){
            System.out.println(ex.toString());
        }
        return listTamu;
    }
    public void insert(String idTamu, String namaDepan, String namaBelakang, String alamat, String no_telepon){
        String query = "INSERT INTO tamu "
                + "(id_tamu,nama_depan,nama_belakang,alamat,no_tlp) "
                + "VALUES "
                + "('"+idTamu+"','"+namaDepan+"','"+namaBelakang+"','"+alamat+"','"+no_telepon+"')";
        try{
            Statement stm = con.createStatement();
            int result = stm.executeUpdate(query);
        } catch (SQLException ex){
            System.out.println(ex.toString());
        }
    }
    public void update(String idTamu, String namaDepan, String namaBelakang, String alamat, String no_telepon){
        String query = "UPDATE tamu set "
                + "nama_depan = '"+namaDepan+"', nama_belakang = '"+namaBelakang+"'"
                + ", alamat = '"+alamat+"',no_tlp = '"+no_telepon+"' WHERE id_tamu = '"+idTamu+"'";
        try{
            Statement stm = con.createStatement();
            int result = stm.executeUpdate(query);
        } catch (SQLException ex){
            System.out.println(ex.toString());
        }
    }
    public void delete(String idTamu){
        String query = "DELETE FROM tamu WHERE id_tamu = '"+ idTamu +"'";
        try{
            Statement stm = con.createStatement();
            int result = stm.executeUpdate(query);
        } catch (SQLException ex){
            System.out.println(ex.toString());
        }
    }
}
