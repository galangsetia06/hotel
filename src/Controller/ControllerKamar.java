package Controller;
import Model.Kamar;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
public class ControllerKamar {
    Scanner in = new Scanner(System.in);
    ConnectionManager conMan = new ConnectionManager();
    Connection con = conMan.LogOn();
    
    public List<Kamar> tampil() {
        List<Kamar> listPrd = new ArrayList<Kamar>();
        try {
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM Kamar");
            while(rs.next()) {
                Kamar prd = new Kamar();
                prd.setIdKamar(rs.getInt("ID_Kamar"));
                prd.setNoKamar(rs.getString("No_Kamar"));
                prd.setNama(rs.getString("Nama"));
                prd.setHarga(rs.getInt("Harga"));
                listPrd.add(prd);
            }
        } catch (SQLException ex){
            System.out.println(ex.toString());
        }
        return listPrd;
    }
    
    public void insert(int id_kamar, String no_kamar, String nama,
            int harga) {
        String query = "INSERT INTO Kamar (ID_Kamar, No_Kamar, Nama, "
                + "Harga) values "
                + "("+id_kamar+",'"+no_kamar+"','"+nama+"',"+harga+")";
        try {
            Statement stm = con.createStatement();
            int result = stm.executeUpdate(query);
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
                
    }
    public void update(int id_kamar, String no_kamar, String nama, int harga){
        String query = "UPDATE kamar SET No_Kamar = '"+ no_kamar +"', Nama = '"+ nama +"', "
                + "Harga = "+ harga +" WHERE id_kamar = "+  id_kamar +"";
        try {
            Statement stm = con.createStatement();
            int result = stm.executeUpdate(query);
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
    }
    public void delete(int id_kamar){
        String query = "DELETE FROM kamar WHERE ID_Kamar = "+id_kamar;
        try {
            Statement stm = con.createStatement();
            int result = stm.executeUpdate(query);
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        
    }
}
