/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;


import Model.Transaksi;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author asus
 */
public class ControllerTransaksi {
    Scanner in = new Scanner(System.in);
    ConnectionManager conMan = new ConnectionManager();
    Connection con = conMan.LogOn();
    
    public void insert(int ID_Transaksi, String ID_Reservasi, String ID_Staff, int Total_pembayaran){
    String query = "Insert into transaksi values ("+ID_Transaksi+",'"+ID_Reservasi+"','"+ID_Staff+"',"+Total_pembayaran+")";
        try{
            Statement stm = con.createStatement();
            int result = stm.executeUpdate(query);
        }catch (SQLException ex){
            System.out.println(ex);
            System.out.println("Input Gagal");
        }
    }
    public int getTotal(String ID_Reservasi){
        int Total = 0;
        String query = "SELECT Harga from reservasi WHERE ID_Reservasi = '"+ID_Reservasi+"'";
        
        try {
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery(query);
            while(rs.next()){
                Total = rs.getInt("Harga");
            }
        } catch (SQLException e) {
            
            System.out.println(e.toString());
        }
        return Total;
    }
    public List<Transaksi> cari (String key){
        List<Transaksi> listTransaksi = new ArrayList();
        try{
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM transaksi WHERE ID_Transaksi = '"+key+"'");
            while (rs.next()){
                Transaksi Transaksi = new Transaksi();
                Transaksi.setID_Transaksi(rs.getInt("ID_Transaksi"));
                Transaksi.setID_Reservasi(rs.getString("ID_Reservasi"));
                Transaksi.setID_Staff(rs.getString("ID_Staff"));
                Transaksi.setTotal_pembayaran(rs.getInt("Total_pembayaran"));
                listTransaksi.add(Transaksi);
            }
        } catch (SQLException ex){
            System.out.println(ex.toString());
        }
        return listTransaksi;
    }
    
    
    public List<Transaksi> tampil(){
        List<Transaksi> listTransaksi = new ArrayList();
            try{
                Statement stm = con.createStatement();
                ResultSet rs = stm.executeQuery("SELECT * FROM transaksi");
                while (rs.next()){
                    Transaksi Transaksi = new Transaksi();
                    Transaksi.setID_Transaksi(rs.getInt("ID_Transaksi"));
                    Transaksi.setID_Reservasi(rs.getString("ID_Reservasi"));
                    Transaksi.setID_Staff(rs.getString("ID_Staff"));
                    Transaksi.setTotal_pembayaran(rs.getInt("Total_pembayaran"));
                    listTransaksi.add(Transaksi);
                }
           }catch (SQLException ex){
           System.out.println(ex.toString());
        }
        return listTransaksi;
    }

  public void update(int ID_Transaksi, String ID_Reservasi, String ID_Staff, int Total_pembayaran){
        int result = 0;
        String query = "UPDATE transaksi set ID_Reservasi = '"+ID_Reservasi+"', "
                + "ID_Staff = '"+ID_Staff+"', "
                + "Total_pembayaran = "+Total_pembayaran
                + " WHERE ID_Transaksi = "+ID_Transaksi;
        try {
            Statement stm = con.createStatement();
            result = stm.executeUpdate(query);
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
    }
    
    public void delete(int ID_Transaksi) {
        
        int result = 0;
        String query = "DELETE FROM transaksi WHERE ID_Transaksi = "+ID_Transaksi;
        try {
            
            Statement stm = con.createStatement();
            result = stm.executeUpdate(query);
        } catch (SQLException ex) {
            
            System.out.println(ex.toString());
        }
    }
}
