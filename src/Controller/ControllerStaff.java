package Controller;
import Model.Staff;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ControllerStaff {
    Scanner in = new Scanner(System.in);
    ConnectionManager conMan = new ConnectionManager();
    Connection con = conMan.LogOn();
    
 public void insertStaff(String Id_Staff, String username, String password, String Nama_Depan,
            String Nama_Belakang, String Alamat, String No_Telepon) {
       String query = "INSERT INTO staff (id_staff, username, password, nama_depan, nama_belakang, alamat, no_tlp) values " 
                + "('"+Id_Staff+"', '"+username+"','"+password+"','"+Nama_Depan+"','"+Nama_Belakang+"'," 
                + "'"+Alamat+"','"+No_Telepon+"')"; 
        try {
            Statement stm = con.createStatement();
            int result = stm.executeUpdate(query);
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
                
    }
public List<Staff> tampil() {
        List<Staff> listStf = new ArrayList();
        try {
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM staff");
            while(rs.next()) {
                Staff Stf = new Staff();
                Stf.setId_staff(rs.getString("id_staff"));
                Stf.setUsername(rs.getString("username"));
                Stf.setPassword(rs.getString("password"));
                Stf.setNama_depan(rs.getString("nama_depan"));
                Stf.setNama_belakang(rs.getString("nama_belakang"));
                Stf.setAlamat(rs.getString("alamat"));
                Stf.setNo_tlp(rs.getString("no_tlp"));
                listStf.add(Stf);
            }
        } catch (SQLException ex){
            System.out.println(ex.toString());
        }
        return listStf;
    }
public boolean login(String Username, String Password){
        boolean authKey = false;
        try {
            String query = "SELECT * FROM staff WHERE username = '"+Username+"' AND password = '"+Password+"'";
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery(query);
            while(rs.next()){
                Staff staff = new Staff();
                staff.setUsername(rs.getString("username"));
                staff.setPassword(rs.getString("password"));
                if(Username.equals(rs.getString("username")) && Password.equals(rs.getString("password"))){
                    authKey = true;
                }
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        }
        return authKey;
    }
public void updateStaff (String Id_Staff, String username, String password, String Nama_Depan,
            String Nama_Belakang, String Alamat, String No_Telepon){ 
        int result = 0; 
      String query = "UPDATE staff set username = '" + username + "', "
                + "password= '" + password + "', "
                + "nama_depan= '" + Nama_Depan + "', "
                + "nama_belakang='" + Nama_Belakang + "',alamat ='" + Alamat + "', "
                + "no_tlp= '" + No_Telepon + "'"
                + "WHERE id_staff = '" + Id_Staff + "'";
      try {
          System.out.println(query);
            Statement stm = con.createStatement();
           result = stm.executeUpdate(query);
        } catch (SQLException ex) {
            System.out.println("Update Gagal");
        }
    }

 public void deleteStaff(String Id_Staff){
        int result = 0;
        String query = "Delete From staff WHERE id_staff =  '"+Id_Staff+"' "; 
          try {
            Statement stm = con.createStatement();
            result = stm.executeUpdate(query);
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
   }

}
