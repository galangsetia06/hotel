/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.TipePembayaran;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class ControllerTipePembayaran {
    Scanner sc = new Scanner(System.in);
    ConnectionManager conMan = new ConnectionManager();
    Connection con = conMan.LogOn();
    
    public void insertTipePembayaran(String idReservasi, String jenisPembayaran, String noRekening) {
        String query = "INSERT INTO tipepembayaran (idReservasi, jenisPembayaran, no_rekening) VALUES "
                + "('" + idReservasi + "', '" + jenisPembayaran + "', '" +  noRekening+ "')";
        try {
            Statement stm = con.createStatement();
            int result = stm.executeUpdate(query);
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
    }
    
    public List<TipePembayaran> tampil() {

        List<TipePembayaran> listTpm = new ArrayList();
        try {
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM tipepembayaran");
            while (rs.next()) {
                TipePembayaran tpm = new TipePembayaran();
                tpm.setIdReservasi(rs.getString("idReservasi"));
                tpm.setJenisPembayaran(rs.getString("jenisPembayaran"));
                tpm.setNoRekening(rs.getString("no_rekening"));
                listTpm.add(tpm);
            }
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
        return listTpm;
    }
    
    public void update(String idReservasi, String jenisPembayaran, String noRekening) {

        int result = 0;

        String query = "UPDATE tipepembayaran set jenisPembayaran = '" + jenisPembayaran + "', no_rekening = ' " + noRekening + "'" 
                + " WHERE idReservasi = '" + idReservasi + "'";

        try {

            Statement stm = con.createStatement();
            result = stm.executeUpdate(query);
        } catch (SQLException ex) {
            System.out.println(ex.toString());
        }
    }

    public void delete(String idReservasi) {

        int result = 0;
        String query = "DELETE FROM tipepembayaran WHERE idReservasi = '" + idReservasi + "'";
        try {

            Statement stm = con.createStatement();
            result = stm.executeUpdate(query);
        } catch (SQLException ex) {

            System.out.println(ex.toString());
        }
    }
    
    public List<TipePembayaran> cari(String key){
        List<TipePembayaran> listTpm = new ArrayList();
        try{
            Statement stm = con.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * FROM tipepembayaran where idReservasi like %" + key + "% or jenisPembayaran like '%" + key + "%'");
            while(rs.next()){
                TipePembayaran tpm = new TipePembayaran();
                tpm.setIdReservasi(rs.getString("idReservasi"));
                tpm.setJenisPembayaran(rs.getString("jenisPembayaran"));
                tpm.setJenisPembayaran(rs.getString("no_rekening"));
                listTpm.add(tpm);
            }
        } catch (SQLException ex){
            System.out.println(ex.toString());
        }
        return listTpm;
    }
    
}
